import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaisSmall } from '../../interfaces/paises.interfaces';
import { PaisesServicesService } from '../../sevices/paises-services.service';
import { switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-selector-page',
  templateUrl: './selector-page.component.html',
  styleUrls: ['./selector-page.component.css'],
})
export class SelectorPageComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({
    region: ['', [Validators.required]],
    pais: ['', [Validators.required]],
    frontera: ['', [Validators.required]],
  });

  regiones: string[] = [];
  paises: PaisSmall[] = [];
  fronteras: string[] = [];

  cargando: boolean = false;

  constructor(private fb: FormBuilder, private ps: PaisesServicesService) {}

  ngOnInit(): void {
    this.regiones = this.ps.regiones;
    if (this.regiones === []) {
      console.log('vacio');
    }

    this.miFormulario
      .get('region')
      ?.valueChanges.pipe(
        tap((_) => {
          this.miFormulario.get('pais')?.reset('');
          this.cargando = true;
        }),
        switchMap((region) => this.ps.getPaisesPorregion(region))
      )
      .subscribe((paises) => {
        this.paises = paises;
        this.cargando = false;
      });
    this.miFormulario
      .get('pais')
      ?.valueChanges.pipe(
        tap(() => {
          this.fronteras = [];
          this.miFormulario.get('frontera')?.reset('');
          this.cargando = true;
        }),
        switchMap((codigo) => this.ps.getPaisPorCodigo(codigo))
      )
      .subscribe((pais) => {
        console.log(pais?.borders);
        this.fronteras = pais?.borders || [];
        this.cargando = false;
      });
  }
  guardar() {
    console.log(this.miFormulario.value);
  }
}
