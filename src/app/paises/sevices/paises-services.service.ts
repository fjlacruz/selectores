import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Pais, PaisSmall } from '../interfaces/paises.interfaces';

@Injectable({
  providedIn: 'root',
})
export class PaisesServicesService {
  private baseUrl: string = 'https://restcountries.eu/rest/v2';
  private _reginones: string[] = [
    'Africa',
    'Americas',
    'Asia',
    'Europe',
    'Oceania',
  ];
  get regiones(): string[] {
    return [...this._reginones];
  }

  constructor(private http: HttpClient) {}

  getPaisesPorregion(region: string[]): Observable<PaisSmall[]> {
    return this.http.get<PaisSmall[]>(
      `${this.baseUrl}/region/${region}?fields=alpha3Code;name`
    );
  }
  getPaisPorCodigo(codigo: string): Observable<Pais | null> {
    if (!codigo) {
      return of(null);
    }
    return this.http.get<Pais>(`${this.baseUrl}/alpha/${codigo}`);
  }
}
